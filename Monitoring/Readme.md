# ELK / Prometheus / Grafana

## ELK
- [Официальная документация Elasticsearch](https://www.elastic.co/guide/en/elasticsearch/reference/current/index.html)
- [Официальная документация Kibana](https://www.elastic.co/guide/en/kibana/current/index.html)
- [Официальная документация Elastic APM](https://www.elastic.co/guide/en/apm/guide/current/apm-overview.html)
- [Установка ELK](https://serveradmin.ru/ustanovka-i-nastroyka-elasticsearch-logstash-kibana-elk-stack/)

## Prometheus
- [Документация Prometheus](https://prometheus.io/docs/introduction/overview/)
- [Руководство по Prometheus](https://habr.com/ru/company/southbridge/blog/455290/)
- [Введение в мониторинг серверов](https://habr.com/ru/post/652185/)
- [Мониторинг сервисов с Prometheus](https://habr.com/ru/company/selectel/blog/275803/)
- Видео: [How Prometheus Monitoring works](https://www.youtube.com/watch?v=h4Sl21AKiDg)

## Grafana
- [Документация Grafana](https://grafana.com/docs/grafana/latest/)
- [Вкратце о Grafana](https://habr.com/ru/company/southbridge/blog/431122/)


## Loki
- [Установка и использование Grafana Loki на Linux](https://www.dmosk.ru/instruktions.php?object=grafana-loki)
- [Loki и Grafana](https://habr.com/ru/company/badoo/blog/507718/)
- [Loki - сбор логов в Kubernetes](https://habr.com/ru/company/otus/blog/487118/)
- [Пошаговое руководство](https://infoit.com.ua/linux/kak-pereslat-logi-v-grafana-loki-s-pomoshhyu-promtail/)

## Opentelemetry
- [Документация OpenTelemetry](https://opentelemetry.io/docs/)
- [Opentelemetry Docker Demo](https://opentelemetry.io/docs/demo/docker-deployment/)
- [OpenTelemetry Operator](https://github.com/open-telemetry/opentelemetry-operator)